<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POS System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>

    <div class="contener">
        <div class=" d-flex justify-content-center align-items-center">
            <form class="row g-3 w-50 vh-100 p-5" action="../Create.php" method="post">
                <div class="col-md-12">
                    <h1>Product</h1>
                </div>
                <div class="col-md-12">
                    <label for="inputEmail4" class="form-label">Poducr ID</label>
                    <input type="number" class="form-control" id="ccl" placeholder="id" name="id">
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">Product Name</label>
                    <input type="text" class="form-control" id="ccl1" placeholder="name" name="name">
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">Category</label>
                    <input type="text" class="form-control" id="ccl2" placeholder="category" name="category">
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">Price</label>
                    <input type="number" class="form-control" id="ccl3" placeholder="price" name="price">
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">Quantity</label>
                    <input type="number" class="form-control" id="ccl4" placeholder="qty" name="qty">
                </div>
               
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Add</button>
                    <button type="submit" id="clear" class="btn btn-success">Clear</button>
                </div>
            </form>
        </div>
    </div>




    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script>
        document.getElementById('clear').addEventListener('click',function(e){
            e.preventDefault();

            document.querySelector('#ccl').value='';
            document.querySelector('#ccl1').value='';
            document.querySelector('#ccl2').value='';
            document.querySelector('#ccl3').value='';
            document.querySelector('#ccl4').value='';
            document.querySelector('#ccl5').value='';
        });
    </script>
</body>

</html>