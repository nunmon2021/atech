<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POS System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>

    <?php 
    $db=new mysqli("localhost","root","","pos");

    if($db->connect_error){
        die("Conection fail !! :".$db->connect_error);
    }
    
    $resul=$db->query("SELECT * FROM tbProduct;");

    if(!$resul){
        echo "return data faild !";
    }
    
    ?>

    <div class="contener">
        <div class=" d-flex justify-content-center vh-100  align-items-center">
            <form class="row g-3 w-50 vh-80 p-5" action="../Create_Tax.php" method="post">
                <div class="col-md-12">
                    <h1>Tax</h1>
                </div>
                <div class="col-md-12 p-0">
                    <label for="inputEmail4" class="form-label">Product Name</label>
                    <select name="applay" id="" class="form-control">
                    <option value="fale">select</option>
                        <option value="all">All</option>
                        <?php 
                            foreach($resul as $el){
                                echo "<option value='$el[pname]'>$el[pname]</option>";
                            }
                        ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">tax</label>
                    <input type="number" class="form-control" id="ccl1" placeholder="X %" name="tax">
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">Applay for</label>
                    <select name="status" id="" class="form-control">
                    <option value="fale">select</option>
                        <option value="one">only one</option>
                        <option value="all">All</option>
                    </select>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Applay</button>
                    <button type="submit" id="clear" class="btn btn-success">Clear</button>
                </div>
            </form>
        </div>
    </div>




    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script>
        document.getElementById('clear').addEventListener('click',function(e){
            e.preventDefault();

            document.querySelector('#ccl').value='';
            document.querySelector('#ccl1').value='';
        });
    </script>
</body>

</html>