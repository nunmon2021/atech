<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POS System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>

    <div class="contener">
        <div class=" d-flex justify-content-center align-items-center">
            <form class="row g-3 w-50 vh-100 p-5">
                <div class="col-md-12">
                    <h1>Discount</h1>
                </div>
                <div class="col-md-12">
                    <label for="inputPassword4" class="form-label">Discount for</label>
                    <select name="name" id="" class="form-control">
                        <option value="fale">select</option>
                        <option value="fale">only one</option>
                        <option value="true">All</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">Discount</label>
                    <input type="number" class="form-control" id="ccl2" placeholder="$$$">
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">Discount for</label>
                    <select name="applay" id="" class="form-control">
                        <option value="fale">select</option>
                        <option value="fale">only one</option>
                        <option value="true">All</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">Start date</label>
                    <input type="date" class="form-control" id="ccl4" placeholder="qty">
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">End date</label>
                    <input type="date" class="form-control" id="ccl5" placeholder="total">
                </div>
               
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Add Discount</button>
                    <button type="submit" id="clear" class="btn btn-success">Clear</button>
                </div>
            </form>
        </div>
    </div>




    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script>
        document.getElementById('clear').addEventListener('click',function(e){
            e.preventDefault();

       
            document.querySelector('#ccl2').value='';
            document.querySelector('#ccl4').value='';
            document.querySelector('#ccl5').value='';
        });
    </script>
</body>

</html>