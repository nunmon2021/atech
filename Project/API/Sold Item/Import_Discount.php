<?php

header("Access-Control-Allow-Origin:*");
header("Content-Type:application/json");
header("Access-Control-Allow-Methods:POST");
header("Access-Constrol-Allow-Headers:Content-Type,Access-Control-Allow-Headers,Authorization,X-request-With");

$db=new mysqli("localhost","root","","sold_item");

$data=json_decode(file_get_contents("php://input"));

if($db->connect_error){
    die("connention faild !");
}else{

    if($data->Status=='all'){

        $product=$db->query("SELECT * FROM produc");
        
        if($product){
            $insert=$db->prepare("INSERT INTO discount(discount,startDate,endDate,Status)Values(?,?,?,?)");
            $insert->bind_param("ssss",$data->Discount,$data->StartDate,$data->EndDate,$data->Status);
            $insert->execute();

            $dis=$db->query("SELECT * FROM discount Where Status='all' and EndDate > now()");

            $ttx=0;

            foreach($dis as $el){
                $ttx=$el['disID'];
                // echo "{'mm':'$ttx'}";
            }

            foreach($product as $el){
                
                $Amount=($el['price']*$data->Discount/100);
                $Tax_Detail=$db->prepare("INSERT INTO discountdetail(code,disID,DisAmount) Values(?,?,?);");
                $Tax_Detail->bind_param("sss",$el['code'],$ttx,$Amount);
                $Tax_Detail->execute();

                $disdb=$db->query("SELECT * FROM discount");
                $disid=0;

                foreach($disdb as $els){
                    $disid=$els['disID'];
                }

                $tacktax=$db->query("SELECT * FROM taxdetail Where code=$el[code]");

                $taxValue=0;
                foreach($tacktax as $elem){
                    $taxValue=$elem['TaxAmount'];

                    // echo "{'mm':'$total'}";
                }

                $Amount1=($el['price']+$taxValue)-($el['price']*$data->Discount/100);
                // echo "{'mm':$Amount1'}";
                $amount=$db->query("UPDATE amount SET amount=$Amount1,disID=$disid where code=$el[code]");
              
          }
            
            $insert->close();
            $Tax_Detail->close();
        }else{
            echo "You don't have any product !";
        }

    }elseif($data->Status=="reset"){  //=================>

        $product=$db->query("SELECT * FROM produc");

        $disDetail=$db->query("DELETE FROM discountdetail where disID>0");
       
        foreach($product as $el){

            $tacktax=$db->query("SELECT * FROM taxdetail Where code=$el[code]");

            $taxValue=0;
            foreach($tacktax as $elem){
                $taxValue=$elem['TaxAmount'];

                // echo "{'mm':'$total'}";
            }

            $Amount1=($el['price']+$taxValue);
            //  echo "{'mm':$Amount1'}";
             $amount=$db->query("UPDATE amount SET amount=$Amount1,disID=null where code=$el[code]");
      }
        

    } else{

        $product=$db->query("SELECT * FROM produc where name='$data->name'");
        
        if($product){
            $insert=$db->prepare("INSERT INTO discount(discount,startDate,endDate,Status)Values(?,?,?,?)");
            $insert->bind_param("ssss",$data->Discount,$data->StartDate,$data->EndDate,$data->Status);
            $insert->execute();

            $dis=$db->query("SELECT * FROM discount Where Status='one' and EndDate > now()");

            $ttx=0;

            foreach($dis as $el){
                $ttx=$el['disID'];
                // echo "{'mm':'$ttx'}";
            }

            foreach($product as $el){
                
                $Amount=($el['price']*$data->Discount/100);
                $Tax_Detail=$db->prepare("INSERT INTO discountdetail(code,disID,DisAmount) Values(?,?,?);");
                $Tax_Detail->bind_param("sss",$el['code'],$ttx,$Amount);
                $Tax_Detail->execute();

                $disdb=$db->query("SELECT * FROM discount");
                $disid=0;

                foreach($disdb as $els){
                    $disid=$els['disID'];
                }

                $tacktax=$db->query("SELECT * FROM taxdetail Where code=$el[code]");

                $taxValue=0;
                foreach($tacktax as $elem){
                    $taxValue=$elem['TaxAmount'];

                    // echo "{'mm':'$total'}";
                }

                $Amount1=($el['price']+$taxValue)-($el['price']*$data->Discount/100);
                //  echo "{'mm':$Amount1'}";
                 $amount=$db->query("UPDATE amount SET amount=$Amount1,disID=$disid where code=$el[code]");
              
          }
          
            $insert->close();
            $Tax_Detail->close();
        }else{
            echo "You don't have any product !";
        }
    }


}