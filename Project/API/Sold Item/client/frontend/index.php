<?php

$url='http://phanun.php.kh:8080/Project/API/Sold%20Item/Export_Data.php';

$curl=curl_init($url);

 curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);

curl_setopt($curl,CURLOPT_HTTPHEADER,['Content-Type: application/json']);

$response=curl_exec($curl);



curl_close($curl);

$data=json_decode($response);

//======>>

$url1='http://phanun.php.kh:8080/Project/API/Sold%20Item/Export_Data_not_Discount.php';

$curl1=curl_init($url1);

 curl_setopt($curl1,CURLOPT_RETURNTRANSFER,true);

curl_setopt($curl1,CURLOPT_HTTPHEADER,['Content-Type: application/json']);

$response1=curl_exec($curl1);



curl_close($curl1);

$data1=json_decode($response1);

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Sold Items</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/style1.css" rel="stylesheet" />
    </head>


    <body id="page-top">
        <!-- Navigation-->

        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="#page-top">SOLD ITEMS</a>
                <button class="navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                      <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="index.php">Home</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="product.php">Products</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="tax.php">Tax</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="discount.php">Discount</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Masthead-->
        <header class="masthead bg-primary text-white text-center mx-0 my-0 p-0 pt-5" style="height:500px;">
          <div class="container d-flex align-items-center flex-column pt-5" >
          <!-- Masthead Avatar Image-->
                <img class="masthead-avatar mb-0 mt-0 pt-3" src="assets/img/avataaars.svg" alt="..." style="width:10%; margin-top: -2rem;" />
                <input class="search" type="search" placeholder="Search..." style="width:30%;">
                <input class="btn btnSearch text-white" type="submit" value="Search">
          </div>


            <div class="container d-flex align-items-center flex-column">
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>

            </div>
            <!-- <button class="btn btnNew text-white" value="">add new
            <button class="btn btnUpd text-white" value="">edit
            <button class="btn btnDel text-white" value="">delete -->
        </header>

        <!-- Table -->
        <div class="container my-5">
          <table class="table table-striped">
            <thead class='text-center align-middle'>
              <tr ><th><input type="checkbox"></th>
                  <th>CODE</th>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Price</th>
                  <th>Tax</th>
                  <th>Price Tax</th>
                  <th>Discount</th>
                  <th>Price Discount</th>
                  <th>Amount</th>
              </tr>
            </thead>
            <tbody class='text-center align-middle'>
                <?php
                    foreach($data1 as $el){
                        echo "<tr><td><input type='checkbox'></td>
                            <td>$el->code </td>
                            <td>$el->name</td>
                            <td>$el->caegory</td>
                            <td>$el->price $</td>
                            <td>$el->Tax %</td>
                            <td>$el->TaxAmount $</td>
                            <td>...</td>
                            <td>... $</td>
                            <td>$el->amount $</td>
                            </tr> ";
                        }
                    foreach($data as $el){
                    echo "<tr><td><input type='checkbox'></td>
                        <td>$el->code </td>
                        <td>$el->name</td>
                        <td>$el->caegory</td>
                        <td>$el->price $</td>
                        <td>$el->Tax %</td>
                        <td>$el->TaxAmount $</td>
                        <td>$el->discount %</td>
                        <td>$el->DisAmount $</td>
                        <td>$el->amount $</td>
                        </tr> ";
                    }
                ?>
            </tbody>
            <tfoot>
              <tr><th></th>
                <th>CODE</th>
                <th>Name</th>
                <th>Category</th>
                <th>Price</th>
                <th>Tax</th>
                <th>Price Tax</th>
                <th>Discount</th>
                <th>Price Discount</th>
                <th>Amount</th>
              </tr>
            </tfoot>
          </table>
        </div>

        <!-- Footer-->
        <footer class="footer text-center">
            <div class="container">
                <div class="row">
                    <!-- Footer Location-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Location</h4>
                        <p class="lead mb-0">
                            2215 John Daniel Drive
                            <br />
                            Clark, MO 65243
                        </p>
                    </div>
                    <!-- Footer Social Icons-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Around the Web</h4>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-twitter"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-linkedin-in"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-dribbble"></i></a>
                    </div>
                    <!-- Footer About Text-->
                    <div class="col-lg-4">
                        <h4 class="text-uppercase mb-4">About Freelancer</h4>
                        <p class="lead mb-0">
                            Freelance is a free to use, MIT licensed Bootstrap theme created by
                            <a href="http://startbootstrap.com">Start Bootstrap</a>
                            .
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright &copy; Your Website 2022</small></div>
        </div>
        <!-- Portfolio Modals-->
        <!-- Portfolio Modal 1-->
       
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>
