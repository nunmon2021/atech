<?php

$url='http://phanun.php.kh:8080/Project/API/Sold%20Item/Export_Tax.php';

$curl=curl_init($url);

 curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);

curl_setopt($curl,CURLOPT_HTTPHEADER,['Content-Type: application/json']);

$response=curl_exec($curl);



curl_close($curl);

$data=json_decode($response);


$db = new mysqli("localhost", "root", "", "sold_item");

// $data=json_decode(file_get_contents("php://input"));

if ($db->connect_error) {
    die("connention faild !");
} else {

    $sql = "SELECT * FROM produc";


    $getbase = $db->query($sql);


    if(!$getbase){
        echo "{'mes':}'select data faild !";
    }else{
        $file=mysqli_fetch_all($getbase,MYSQLI_ASSOC);
    }
}


?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Sold Items</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/style1.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->

        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="#page-top">Tax</a>
                <button class="navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                      <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="index.php">Home</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="product.php">Products</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="tax.php">Tax</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="discount.php">Discount</a></li>
                    </ul>
                </div>
            </div>
            <!-- <div class="tool">
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                      <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="#portfolio">Home</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="#portfolio">Products</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="#about">Tax</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="#contact">Discount</a></li>
                    </ul>
                </div>
            </div> -->
        </nav>

        <!-- Masthead-->
        <header class="masthead bg-primary text-white text-center mx-0 my-0 p-0 pt-5" style="height:500px;">
          <div class="container d-flex align-items-center flex-column pt-5">
          <!-- Masthead Avatar Image-->
                <img class="masthead-avatar mb-0 pt-3" src="assets/img/avataaars.svg" alt="..." style="width:10%; "/>
                <input class="search" type="search" placeholder="Search..." style="width:30%;">
                <input class="btn btnSearch text-white" type="submit" value="Search">
          </div>
            <div class="container d-flex align-items-center flex-column">
                <!-- Icon Divider-->
                <!-- <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div> -->
                <button class="btn btnNew text-white float-right" data-bs-toggle="modal" data-bs-target="#portfolioModal1" value="">ADD NEW</button>
            </div>
            <!-- <button class="btn btnNew text-white" value="">ADD NEW
            <button class="btn btnUpd text-white" value="">EDIT
            <button class="btn btnDel text-white" value="">DELETE -->
        </header>

        <!-- Pop-up forms -->
        <div class="popup">
          <div class="popup-content">
            <form class="myForm text-white">
              <h1>Insert Tax Value</h1>
              <label for="txtTaxType">TYPE</label>
              <input type="text" name="txtTaxType">
              <label for="txtTax">Tax</label>
              <input type="number" name="txtTax">
              <label for="txtName">Name</label>
              <input type="text" name="txtName">
              <label for="txtStartDate">Start Date</label>
              <input type="date" name="txtStartDate">
              <label for="txtEndDate">End Date</label>
              <input type="date" name="txtEndDate">
              <label for="txtSts">Status</label>
              <input type="text" name="txtSts">

              <input class="btnInsert" type="button" value="Insert">
              <input class="btnCancel" type="button" value="Cancel">
            </form>
          </div>
        </div>

        <!-- Table -->
        <div class="container my-5">
          <table class="table table-striped">
            <thead class='text-center align-middle'>
              <tr><th><input type="checkbox"></th>
                  <th>ID</th>
                  <th>Name</th>
                  <th>TYPE</th>
                  <th>Tax</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Tax Amount</th>
                 
                  <th>....</th>
              </tr>
            </thead>
            <tbody class='text-center align-middle'>
                <?php

                        $i=1;
                    foreach($data as $el){
                        ++$i;
                        echo "<tr><td><input type='checkbox'></td>
                        <td>$el->Id</td>
                        <td>$el->name</td>
                        <td>$el->Type</td>
                        <td>$el->Tax %</td>
                        <td>$el->StartDate</td>
                        <td>$el->EndDate</td>
                        <td>$el->TaxAmount $</td>
                       
                        <td>
                        <button class='btn btnNew text-white float-right' data-bs-toggle='modal' data-bs-target='#portfolioModal$i'>Edit</button>
                        </td>



                        <td>
                        <div class='portfolio-modal modal fade' id='portfolioModal$i' tabindex='-1' aria-labelledby='portfolioModal1' aria-hidden='true'>
                            <div class='modal-dialog modal-lg'>
                                <div class='modal-content'>
                                    <div class='modal-header border-0'><button class='btn-close' type='button' data-bs-dismiss='modal' aria-label='Close'></button></div>
                                    <div class='modal-body text-center pb-8'>
                                        <div class='container'>
                                            <div class='row justify-content-center'>
                                                <div class='col-lg-8'>
                                                
                                                <div class=' d-flex justify-content-center align-items-center'>
                                                    <form class='row g-3 w-100 vh-40 p-0' action='../Request/Update/Update_Tax.php' method='post'>
                                                        <div class='col-md-12'>
                                                            <h1>Edt Tax</h1>
                                                        </div>
                                                        <input type='text' class='form-control' id='ccl' value='$el->Id' hidden placeholder='Type' name='Id'>
                                                        <div class='col-md-12'>
                                                            <label for='inputEmail4' class='form-label'>Type</label>
                                                            <input type='text' class='form-control' id='ccl' value='$el->Type' placeholder='Type' name='Type'>
                                                        </div>
                                                        <div class='col-md-6'>
                                                            <label for='inputPassword4' class='form-label'>Tax</label>
                                                            <input type='text' class='form-control' id='ccl1' value='$el->Tax' placeholder='Tax' name='Tax'>
                                                        </div>
                                                        <div class='col-md-6'>
                                                            <label for='inputPassword4' class='form-label'>Product name</label>
                                                            <input type='text' class='form-control' id='ccl1' value='$el->name' placeholder='Tax' name='name'>
                                                        
                                                        </div>
                                                        <div class='col-md-6'>
                                                            <label for='inputPassword4' class='form-label'>Start Date</label>
                                                            <input type='date' class='form-control' id='ccl3' value='$el->StartDate' placeholder='StartDate' name='StartDate'>
                                                        </div>
                                                        <div class='col-md-6'>
                                                            <label for='inputPassword4' class='form-label'>End Date</label>
                                                            <input type='date' class='form-control' id='ccl3' value='$el->EndDate' placeholder='EndDate' name='EndDate'>
                                                        </div>
                                                        <div class='col-md-6'>
                                                            <label for='inputPassword4' class='form-label'>Tax for</label>
                                                           
                                                            <input type='text' class='form-control' id='ccl1' value='$el->Status' placeholder='Tax' name='Status'>
                                                        </div>
                                                    
                                                        <div class='col-12'>
                                                            <button type='submit' class='btn btn-primary' style='color:green;'>Add</button>
                                                            <button type='submit' id='clear' class='btn btn-success' style='color:green;'>Clear</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                
                                                <!-- <button class='btn btn-primary' data-bs-dismiss='modal'>
                                                        <i class='fas fa-xmark fa-fw'></i>
                                                        Close Window
                                                    </button> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </td>
                      </tr>";
                    }
                ?>
              

            </tbody>
            <tfoot>
              <tr><th></th>
                <th>ID</th>
                <th>Name</th>
                <th>TYPE</th>
                <th>Tax</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Tax Amount</th>
                <th>...</th>
              </tr>
            </tfoot>
          </table>
        </div>

        <!-- Footer-->
        <footer class="footer text-center">
            <div class="container">
                <div class="row">
                    <!-- Footer Location-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Location</h4>
                        <p class="lead mb-0">
                            2215 John Daniel Drive
                            <br />
                            Clark, MO 65243
                        </p>
                    </div>
                    <!-- Footer Social Icons-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Around the Web</h4>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-twitter"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-linkedin-in"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-dribbble"></i></a>
                    </div>
                    <!-- Footer About Text-->
                    <div class="col-lg-4">
                        <h4 class="text-uppercase mb-4">About Freelancer</h4>
                        <p class="lead mb-0">
                            Freelance is a free to use, MIT licensed Bootstrap theme created by
                            <a href="http://startbootstrap.com">Start Bootstrap</a>
                            .
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright &copy; Your Website 2022</small></div>
        </div>
        <!-- Portfolio Modals-->
        <!-- Portfolio Modal 1-->
        <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" aria-labelledby="portfolioModal1" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header border-0"><button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                    <div class="modal-body text-center pb-5">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                  
                                <div class=" d-flex justify-content-center align-items-center">
                                    <form  class="row g-3 w-100 vh-70 p-0" action="../Request/Add/Request_Tax.php" method="post">
                                        <div class="col-md-12">
                                            <h1>Tax</h1>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="inputEmail4" class="form-label">Type</label>
                                            <input  type="text" class="form-control" id="ccl" placeholder="Type" name="Type">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="inputPassword4" class="form-label">Tax</label>
                                            <input type="text" class="form-control" id="ccl1" placeholder="Tax" name="Tax">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="inputPassword4" class="form-label">Product name</label>
                                            <select name="name" id="" class="form-control" >
                                                <option value=" ">select</option>
                                                <?php
                                                    foreach($file as $pro){
                                                        echo "<option value='$pro[name]'>$pro[name]</option>";
                                                    }
                                                ?>
                                            </select>
                                         
                                        </div>
                                        <div class="col-md-6">
                                            <label for="inputPassword4" class="form-label">Start Date</label>
                                            <input type="date"  class="form-control" id="ccl3" placeholder="StartDate" name="StartDate">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="inputPassword4" class="form-label">End Date</label>
                                            <input type="date" class="form-control" id="ccl3" placeholder="EndDate" name="EndDate">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="inputPassword4" class="form-label">Tax for</label>
                                            <select name="Status" id="" class="form-control" >
                                                <option value="all">All Product</option>
                                                <option value="one">Ony One Product</option>
                                            </select>
                                         
                                        </div>
                                    
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary" style="color:green;">Add</button>
                                            <button type="submit" id="clear" class="btn btn-success" style="color:green;">Clear</button>
                                        </div>
                                    </form>
                                </div>
                                
                                <!-- <button class="btn btn-primary" data-bs-dismiss="modal">
                                        <i class="fas fa-xmark fa-fw"></i>
                                        Close Window
                                    </button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <script>
            document.getElementById('clear').addEventListener('click',function(e){
            e.preventDefault();

            document.querySelector('#ccl').value='';
            document.querySelector('#ccl1').value='';
            document.querySelector('#ccl2').value='';
            document.querySelector('#ccl3').value='';
            document.querySelector('#ccl4').value='';
            document.querySelector('#ccl5').value='';
        });
    </script>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
        <script>
          document.getElementById('btnNew').addEventListener("click", function(){
            document.querySelector(".popup").style.display = "flex";
          })

          document.querySelector(".btnCancel").addEventListener("click", function(){
            document.querySelector(".popup").style.display = "none";
          })
        </script>
    </body>
</html>
