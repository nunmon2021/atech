<?php
$url='http://phanun.php.kh:8080/Project/API/Sold%20Item/Export_item.php';

$curl=curl_init($url);

 curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);

curl_setopt($curl,CURLOPT_HTTPHEADER,['Content-Type: application/json']);

$response=curl_exec($curl);



curl_close($curl);

$data=json_decode($response);



?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Sold Items</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/style1.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->

        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="#page-top">Product List</a>
                <button class="navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                      <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="index.php">Home</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="product.php">Products</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="tax.php">Tax</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="discount.php">Discount</a></li>
                    </ul>
                </div>
            </div>
            <!-- <div class="tool">
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                      <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="#portfolio">Home</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="#portfolio">Products</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="#about">Tax</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="#contact">Discount</a></li>
                    </ul>
                </div>
            </div> -->
        </nav>

        <!-- Masthead-->
        <header class="masthead bg-primary text-white text-center mx-0 my-0 p-0 pt-5" style="height:500px;">
          <div class="container d-flex align-items-center flex-column pt-5">
          <!-- Masthead Avatar Image-->
                <img class="masthead-avatar mb-0 pt-3" src="assets/img/avataaars.svg" alt="..." style="width:10%; "/>
                <input class="search" type="search" placeholder="Search..." style="width:30%;">
                <input class="btn btnSearch text-white" type="submit" value="Search">
          </div>
            <div class="container d-flex align-items-center flex-column">
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <!-- <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div> -->
                    <button class="btn btnNew text-white float-right" data-bs-toggle="modal" data-bs-target="#portfolioModal1" value="">ADD NEW</button>
                </div>
                <!-- Model -->

                
            </div>
            <!-- <button class="btn btnNew text-white" value="">ADD NEW
            <button class="btn btnUpd text-white" value="">EDIT
            <button class="btn btnDel text-white" value="">DELETE -->
        </header>


        <!-- Table -->
        <div class="container my-5">
          <table class="table table-striped" style="width:100%;">
            <thead>
              <tr class='text-center align-middle'><th><input type="checkbox"></th>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Price</th>
                  <th>...</th>
              </tr>
            </thead>
            <tbody>
            <?php
                $i=1;
                foreach($data as $el){
                    ++$i;
                    echo "<tr class='text-center align-middle'><td><input type='checkbox'></td>
                        <td>$el->code </td>
                        <td>$el->name</td>
                        <td>$el->caegory</td>
                        <td>$el->price $</td>
                        <td>
                        <button class='btn btnNew text-white float-right' data-bs-toggle='modal' data-bs-target='#portfolioModal$i'>Edit</button>
                        </td>
                        <td>
                             <div class='portfolio-modal modal fade' id='portfolioModal$i' tabindex='-1' aria-labelledby='portfolioModal1' aria-hidden='true'>
                                <div class='modal-dialog modal-lg'>
                                    <div class='modal-content'>
                                        <div class='modal-header border-0'><button class='btn-close' type='button' data-bs-dismiss='modal' aria-label='Close'></button></div>
                                        <div class='modal-body text-center pb-5'>
                                            <div class='container'>
                                                <div class='row justify-content-center'>
                                                    <div class='col-lg-8'>
                                                    
                                                    <div class=' d-flex justify-content-center align-items-center'>
                                                        <form class='row g-3 w-100 vh-70 p-0' action='../Request/Update/Update_item.php' method='post'>
                                                            <div class='col-md-12'>
                                                                <h1>Edit Product</h1>
                                                            </div>
                                                            <div class='col-md-12'>
                                                                <label for='inputEmail4' class='form-label'>Poducr code</label>
                                                                <input type='number' class='form-control' id='ccl' value='$el->code' placeholder='id' name='code'>
                                                            </div>
                                                            <div class='col-md-6'>
                                                                <label for='inputPassword4' class='form-label'>Product Name</label>
                                                                <input type='text' class='form-control' id='ccl1' value='$el->name' placeholder='name' name='name'>
                                                            </div>
                                                            <div class='col-md-6'>
                                                                <label for='inputPassword4' class='form-label'>Category</label>
                                                                <input type='text' class='form-control' id='ccl2' value='$el->caegory' placeholder='category' name='category'>
                                                            </div>
                                                            <div class='col-md-6'>
                                                                <label for='inputPassword4' class='form-label'>Price</label>
                                                                <input type='number' class='form-control' id='ccl3' value='$el->price' placeholder='price' name='price'>
                                                            </div>
                                                        
                                                            <div class='col-12'>
                                                                <button type='submit' class='btn btn-primary' style='color:green;'>Add</button>
                                                                <button type='submit' id='clear' class='btn btn-success' style='color:green;'>Clear</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    
                                                
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>


                     </tr> ";
                }
            ?>

            </tbody>
            <tfoot>
              <tr class='text-center align-middle'><th></th>
                <th>ID</th>
                <th>Name</th>
                <th>Category</th>
                <th>Price</th>
                <th>...</th>
              </tr>
            </tfoot>
          </table>
        </div>

        <!-- Footer-->
        <footer class="footer text-center">
            <div class="container">
                <div class="row">
                    <!-- Footer Location-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Location</h4>
                        <p class="lead mb-0">
                            2215 John Daniel Drive
                            <br />
                            Clark, MO 65243
                        </p>
                    </div>
                    <!-- Footer Social Icons-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Around the Web</h4>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-twitter"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-linkedin-in"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-dribbble"></i></a>
                    </div>
                    <!-- Footer About Text-->
                    <div class="col-lg-4">
                        <h4 class="text-uppercase mb-4">About Freelancer</h4>
                        <p class="lead mb-0">
                            Freelance is a free to use, MIT licensed Bootstrap theme created by
                            <a href="http://startbootstrap.com">Start Bootstrap</a>
                            .
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright &copy; Your Website 2022</small></div>
        </div>
        <!-- Portfolio Modals-->
        <!-- Portfolio Modal 1-->
        <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" aria-labelledby="portfolioModal1" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header border-0"><button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                    <div class="modal-body text-center pb-5">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                  
                                <div class=" d-flex justify-content-center align-items-center">
                                    <form class="row g-3 w-100 vh-70 p-0" action="../Request/Add/Request_Add.php" method="post">
                                        <div class="col-md-12">
                                            <h1>Product</h1>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="inputEmail4" class="form-label">Poducr code</label>
                                            <input type="number" class="form-control" id="ccl" placeholder="id" name="code">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="inputPassword4" class="form-label">Product Name</label>
                                            <input type="text" class="form-control" id="ccl1" placeholder="name" name="name">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="inputPassword4" class="form-label">Category</label>
                                            <input type="text" class="form-control" id="ccl2" placeholder="category" name="category">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="inputPassword4" class="form-label">Price</label>
                                            <input type="number" class="form-control" id="ccl3" placeholder="price" name="price">
                                        </div>
                                    
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary" style="color:green;">Add</button>
                                            <button type="submit" id="clear" class="btn btn-success" style="color:green;">Clear</button>
                                        </div>
                                    </form>
                                </div>
                                
                               
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

        <script>
        document.getElementById('clear').addEventListener('click',function(e){
            e.preventDefault();

            document.querySelector('#ccl').value='';
            document.querySelector('#ccl1').value='';
            document.querySelector('#ccl2').value='';
            document.querySelector('#ccl3').value='';
            document.querySelector('#ccl4').value='';
            document.querySelector('#ccl5').value='';
        });
    </script>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>
